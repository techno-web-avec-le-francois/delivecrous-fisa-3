const mongoose = require("mongoose");

const dishScema = new mongoose.Schema({
    dish_name: String,
    price: Number,
    description: String,
    allergenes: Array,
});

const Dish = mongoose.model("Dish", dishScema);

const router = require("express").Router();

router.get("/", (req, res) => {
    Dish.find().then((dishes) => res.json(dishes));
});

router.get("/:id", async(req, res) => {
    Dish.findById(req.params.id)
        .then((dishes) => res.json(dishes))
        .catch(() => res.status(404).end());
});

router.post("/", (req, res) => {
    const dishes = req.body;
    Dish.create(dishes).then(() => res.json(dishes));
});

router.get("/search/:string", async(req, res) => {
    Dish.find({
            $or: [
                { dish_name: { $regex: req.params.string, $options: "i" } },
                { description: { $regex: req.params.string, $options: "i" } },
                { name: { $regex: req.params.string, $options: "i" } },
            ],
        })
        .then((dishes) => res.json(dishes))
        .catch(() => res.status(404).end());
});

module.exports = router;
module.exports.Dish = Dish;