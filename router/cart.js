const mongoose = require('mongoose');
const { Dish } = require('./dishes');
const router = require('express').Router()

const cartSchema = new mongoose.Schema({cartContent: Array, userId: String})
const Cart = mongoose.model('Cart', cartSchema)

const commandSchema = new mongoose.Schema({date: Date,cart: Object, address: Object})
const Command = mongoose.model('Command', commandSchema)

router.get('/', (req, res)=>{
    Cart.find()
        .then(carts=>{
            res.json(carts)
        })
        .catch(() => res.status(404).end());
})

router.get('/:id', (req, res)=>{
    Cart.findById(req.params.id)
        .then(cart =>{
            res.json(cart)
        })
        .catch(() => res.status(404).end());
})

router.post('/', (req, res) =>{
    const cart = req.body
    Cart.create({cartContent: [],userId: cart.userId})
        .then(()=>{
            res.json({cartContent: [],userId: cart.userId})
        })
        .catch(() => res.status(404).end());
})

router.delete('/:id', (req, res)=>{
    Cart.findByIdAndDelete(req.params.id)
        .then(cart => {
            res.json(cart)
        })
        .catch(() => res.status(404).end());
})

router.put('/:id/items', (req, res)=>{
    const item = req.body
    Cart.findById(req.params.id)
    .then(cart =>{
        //verif item
        Dish.findById(item.id)
            .then(dish =>{
                cart.cartContent.push(dish)
                //update cart
                Cart.findByIdAndUpdate(cart._id, {cartContent: cart.cartContent})
                .then(() =>{
                    res.json(cart)
                })

            })
            .catch(() => res.status(404).end());
    })
})

router.delete('/:id/items/:itemId', (req, res)=>{
    const itemId = req.params.itemId
    Cart.findById(req.params.id)
        .then(cart =>{
            //verif item && delete item in cartContent
            console.log(cart);
            if(cart.cartContent.filter(e => e._id.toHexString() === itemId).length === 0){
               throw new Error()
            }
        
            const removeIndx = cart.cartContent.findIndex(dish => dish._id === itemId)
            cart.cartContent.splice(removeIndx, 1)

            Cart.findByIdAndUpdate(cart._id, {cartContent: cart.cartContent})
                .then(resp => {
                    console.log(resp);
                    res.json(cart)
                })
            
    })
    .catch(() => res.status(404).end());
})

router.post('/finalize/:id', (req, res) =>{
    const address = req.body
    const cartId = req.params.id
    Cart.findById(cartId)
        .then(cart =>{
            const command = {date: new Date(), cart: cart, address: address}
            Command.create(command)
                .then(() =>{
                    res.json(command)
                })
        })
        .catch(() => res.status(404).end());
})

module.exports = router