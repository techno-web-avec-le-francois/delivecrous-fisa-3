const express = require('express')

const mongoose = require("mongoose");
const database = "mongodb://localhost:27017/delivecrous";
mongoose.connect(database)

const app = express()

app.use(express.json())

app.use('/cart', require('./router/cart'))
app.use('/dishes', require('./router/dishes'))

app.get("*", (req, res) => {
    res.status(404).json({statusCode: 404, error: "NotFound"}).end()
  })

app.listen(1234)